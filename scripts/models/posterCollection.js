define([
	'jquery',
	'underscore',
	'backbone',
	'parse'
], function(
	$,
	_,
	Backbone,
	Parse
) {
	'use strict';

	var Poster = Parse.Object.extend('Poster');

	var PosterCollection = Parse.Collection.extend({
		model: Poster,
		query: (new Parse.Query(Poster).limit(12)).descending('createdAt')
	});

    return PosterCollection;

});