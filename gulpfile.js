/*!
 * gulp
 * $ npm install gulp-ruby-sass gulp-bower gulp-autoprefixer gulp-minify-css gulp-jshint gulp-concat gulp-uglify gulp-imagemin gulp-notify gulp-rename gulp-livereload gulp-cache del --save-dev

	http://markgoodyear.com/2014/01/getting-started-with-gulp/
 */

// Load plugins
var gulp = require('gulp'),
		sass = require('gulp-ruby-sass'),
		bower = require('gulp-bower'),
		autoprefixer = require('gulp-autoprefixer'),
		minifycss = require('gulp-minify-css'),
		jshint = require('gulp-jshint'),
		uglify = require('gulp-uglify'),
		imagemin = require('gulp-imagemin'),
		rename = require('gulp-rename'),
		concat = require('gulp-concat'),
		notify = require('gulp-notify'),
		cache = require('gulp-cache'),
		del = require('del');

// Styles
gulp.task('styles', function() {
	return gulp.src('scss/**/*.scss')
		.pipe(sass({ style: 'expanded', compass: true }))
		.pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
		.pipe(gulp.dest('css'))
		//.pipe(rename({ suffix: '.min' }))
		//.pipe(minifycss())
		//.pipe(gulp.dest('dist/styles'))
		.pipe(notify({ message: 'Styles task complete' }));
});

// Bower

gulp.task('bower', function() {
	return bower()
		.pipe(gulp.dest('bower_components'));
});

// Scripts
gulp.task('scripts', function() {
	return gulp.src('scripts/**/*.js')
		.pipe(jshint('.jshintrc'))
		.pipe(jshint.reporter('default'))
		.pipe(notify({ message: 'Scripts task complete' }));
});

// Linting
gulp.task('lint', function() {
	return gulp.src('scripts/**/*.js')
		.pipe(jshint())
		.pipe(jshint.reporter('default'))
		.pipe(notify({ message: 'Linting task complete' }));
});

// Clean
gulp.task('clean', function(cb) {
		del(['css','bower_components'], cb);
});

// Default task
gulp.task('default', ['clean'], function() {
		gulp.start('styles', 'scripts');
});

// Watch
gulp.task('watch', function() {

	// Watch .scss files
	gulp.watch('scss/**/*.scss', ['styles']);

	// Watch .js files
	//gulp.watch('scripts/**/*', ['scripts']);

});