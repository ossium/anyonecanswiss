/*global require*/
'use strict';

require.config({
	shim: {
		fancybox: {
			deps: ['jquery'],
			exports: 'fancybox'
		},
		bootstrap: {
			deps: ['jquery'],
			exports: 'jquery'
		},
		handlebars: {
			exports: 'Handlebars'
		},
		parse: {
			deps: ['jquery', 'underscore'],
			exports: 'Parse'
		},
		shuffleLetters: {
			deps: ['jquery'],
			exports: 'shuffleLetters'
		},
		bootstrapModal: {
			deps: ['jquery'],
			exports: 'modal'
		}
	},
	paths: {
		jquery: '../bower_components/jquery/dist/jquery',
		backbone: '../bower_components/backbone/backbone',
		underscore: '../bower_components/lodash/dist/lodash',
		bootstrap: '../bower_components/sass-bootstrap/dist/js/bootstrap',
		handlebars: '../bower_components/handlebars/handlebars',
		text: '../bower_components/requirejs-text/text',
		parse: 'vendor/parse-1.3.1',
		shuffleLetters: 'vendor/jquery.shuffleLetters/assets/js/jquery.shuffleLetters',
		fancybox: '../bower_components/fancyBox/source/jquery.fancybox',
		bootstrapModal: '../bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/modal',
		localstorage: '../bower_components/Backbone.localStorage/backbone.localStorage'
	}
});

require([
	'backbone',
	'routes/router',
	'parse'
], function (Backbone, SwissRouter, Parse) {

	Parse.initialize('ryvZIzJNSXMcBHTa0HkOX5KKYiIr28Bf5pJuVUug', '9NSdh2AsARzLysuHuqzdRxIGTBVs4lGvKTZ9FCR5');

	new SwissRouter();

	Backbone.history.start();

});
