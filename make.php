<?php

	$name = 'SwissPoster.png';

	if (isset($GLOBALS["HTTP_RAW_POST_DATA"])){
		// Get the data
		$rawImage = $GLOBALS['HTTP_RAW_POST_DATA'];
		// Remove the headers (data:,) part.
		// A real application should use them according to needs such as to check image type
		$removeHeaders = substr($rawImage, strpos($rawImage, ",")+1);
		// Need to decode before saving since the data we received is already base64 encoded
		$decode = base64_decode($removeHeaders);
		// Save file. This example uses a hard coded filename for testing,
		// but a real application can specify filename in POST variable
		$fopen = fopen($name, 'wb');

		fwrite($fopen,$decode);

		fclose($fopen);

		echo 'passed';
	}
?>