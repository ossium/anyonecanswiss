define([
	'jquery',
	'underscore',
	'backbone',
	'parse',
	'text!templates/galleryStrip.html',
	'fancybox',
	'models/posterCollection'
], function(
	$,
	_,
	Backbone,
	Parse,
	galleryStripViewTemplate,
	fancybox,
	PosterCollection
) {
	'use strict';


	var GalleryStripView = Backbone.View.extend({

		template: _.template( galleryStripViewTemplate ),

		el: $('#footerGallery .container'),

        initialize: function() {
            this.collection = new PosterCollection;

            this.collection.fetch();

            _.bindAll(this, "renderList");

            this.collection.bind( "reset", this.renderList );
        },		

        renderList: function( collection ) {

            var compiled_template = _.template(galleryStripViewTemplate),
                collection = this.collection,
                $el = $(this.el);

            $el.html( compiled_template( { posters: collection.models } ) );

            this.postRender();

        },
		postRender: function(){

			$('.fancybox',this.$el).fancybox();

		}

	});

	return GalleryStripView;
});