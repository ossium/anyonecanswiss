/*global define*/

define([
	'jquery',
	'underscore',
	'backbone',
	'parse',
	'shuffleLetters',
	'text!templates/mainView.html',
	'bootstrapModal',
	'views/galleryStripView'
], function(
	$,
	_,
	Backbone,
	Parse,
	shuffleLetters,
	mainViewTemplate,
	modal,
	GalleryStripView
) {
	'use strict';

	var MainView = Backbone.View.extend({

		template: _.template( mainViewTemplate ),

		tagName: 'div',

		events: {

			'click #swissmaker': 'swissMaker',
			'click .backgroundSwap img': 'backgroundSwap',
			'click #downloadPoster': 'generateDownloadablePoster',
			'click #addToGallery': 'addToGallery'

		},

		render: function(){

			this.$el.html( this.template );

			this.afterRender();

			return this;

		},

		afterRender: function(){

			$('#posterContents',this.$el).shuffleLetters({
				'text': 'Make Instant Swiss Posters! //////////////// Fun With Friends!'
			});
		},

		swissMaker: function(e){

			e.preventDefault();

			var toBeSwissed = $('#toBeSwissed').val();

			$('#posterContents').shuffleLetters({
				'text': toBeSwissed
			});

			this.createPoster();
		},

		backgroundSwap: function(e){

			$('#poster',this.$el).css('background-image','url('+e.currentTarget.currentSrc+')');
			
			this.createPoster();

		},

		createPoster: function(){

			$('#downloadPoster',this.$el).removeClass('disabled');
			var poster = $('#poster',this.$el).css('background-image');
			var background = poster.replace(/^url\(["']?/, '').replace(/["']?\)$/, '');

			//var bg = background.replace('url(','').replace(')','');
			//var bg = background.replace(/^url\(["']?/, '').replace(/["']?\)$/, '');

			var that = this,
				canvas = document.getElementById('saveCanvas'),
				imageObj = new Image(),
				width = 635,
				height = 635,
				x = 40,
				y = 200,
				maxWidth = 500,
				lineHeight = 70,
				toBeSwissed = $('#toBeSwissed').val(),
				context = canvas.getContext('2d');

			context.font = 'bold 50px Helvetica';
			context.fillStyle = '#fff';

			imageObj.onload = function() {
				context.drawImage(imageObj, 0, 0, width, height);
				that.wrapText(context, toBeSwissed, x, y, maxWidth, lineHeight);

			};

			imageObj.src = background;

		},

		wrapText: function(context, text, x, y, maxWidth, lineHeight) {
			var words = text.split(' ');
			var line = '';

			for(var n = 0; n < words.length; n++) {
				var testLine = line + words[n] + ' ';
				var metrics = context.measureText(testLine);
				var testWidth = metrics.width;
				//context.clearRect(0, 0, 500, 600);
				if(testWidth > maxWidth) {
					context.fillText(line, x, y);
					line = words[n] + ' ';
					y += lineHeight;
				}
				else {
					line = testLine;
				}
			}
			context.fillText(line, x, y);

		},

		addToGallery: function(e){

			if(e){
				e.preventDefault();
			}

			var myDrawing = document.getElementById('saveCanvas');

			var canvasData = myDrawing.toDataURL('image/png');

			var parseFile = new Parse.File("poster.png", { base64: canvasData });

			parseFile.save().then(function() {
				console.log('saved to parse');

				var swissPoster = new Parse.Object("Poster");
				swissPoster.set("posterFile", parseFile);
				swissPoster.save();
				var appGalleryStripView = new GalleryStripView();

			// The file has been saved to Parse.
			}, function(error) {
				console.log('not saved to parse');
			// The file either could not be read, or could not be saved to Parse.
			});

		},

		generateDownloadablePoster: function(e){

			if(e){
				e.preventDefault();
			}			

			var myDrawing = document.getElementById('saveCanvas');

			var canvasData = myDrawing.toDataURL('image/png');

			$.ajax({
				url: 'make.php',
				type: 'POST',
				contentType: 'application/upload',
				data: canvasData
			}).done(function(data){
				$('#downloadModal').modal('show');
			});
		}


	});

	return MainView;
});