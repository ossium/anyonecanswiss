/*global define*/

define([
	'jquery',
	'backbone',
	'parse',
	'text!templates/aboutView.html',
	'text!templates/swissView.html',
	'text!templates/pressView.html',
	'views/mainView',
	'views/galleryStripView'
], function(
	$,
	Backbone,
	Parse,
	aboutView,
	swissView,
	pressView,
	MainView,
	GalleryStripView
) {
	'use strict';

	var SwissRouter = Backbone.Router.extend({
		routes: {
			'make':'showHome',
			'about':'showAbout',
			'swiss':'showSwiss',
			'press':'showPress',
			'*path':  'showHome'
		},


		showHome: function(){

			var appMainView = new MainView();
			var appGalleryStripView = new GalleryStripView();

			$('#content').html( appMainView.render().el );

		},

		showAbout: function(){
			var compiledTemplate = _.template( aboutView );
			$('#content').html( compiledTemplate );
		},

		showSwiss: function(){
			var compiledTemplate = _.template( swissView );
			$('#content').html( compiledTemplate );
		},

		showPress: function(){
			var compiledTemplate = _.template( pressView );
			$('#content').html( compiledTemplate );
		}

	});

	return SwissRouter;
});